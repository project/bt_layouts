((ResponsiveLayout) => {
  fiveColumns = function(layouts, breakpoint) {
    let customProperties = {
      SectionColumnsSize: 'sectionColumnsSize',
      SectionColumnsGap: 'sectionColumnsGap',
      SectionColumn1Order: 'sectionColumn1Order',
      SectionColumn1GridRow: 'sectionColumn1GridRow',
      SectionColumn1AlignItems: 'sectionColumn1AlignItems',
      SectionColumn2Order: 'sectionColumn2Order',
      SectionColumn2GridRow: 'sectionColumn2GridRow',
      SectionColumn2AlignItems: 'sectionColumn2AlignItems',
      SectionColumn3Order: 'sectionColumn3Order',
      SectionColumn3GridRow: 'sectionColumn3GridRow',
      SectionColumn3AlignItems: 'sectionColumn3AlignItems',
      SectionColumn4Order: 'sectionColumn4Order',
      SectionColumn4GridRow: 'sectionColumn4GridRow',
      SectionColumn4AlignItems: 'sectionColumn4AlignItems',
      SectionColumn5Order: 'sectionColumn5Order',
      SectionColumn5GridRow: 'sectionColumn5GridRow',
      SectionColumn5AlignItems: 'sectionColumn5AlignItems'
    }

    let i = 0
    for (i; i < layouts.length; i++) {
      ResponsiveLayout.applyProperties(layouts[i], breakpoint, customProperties)
    }
  }

  ResponsiveLayout.layoutsRegister('five', fiveColumns)

})(ResponsiveLayout)
