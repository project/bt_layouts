((ResponsiveLayout) => {
  oneColumn = function(layouts, breakpoint) {
    let customProperties = {
      SectionColumn1GridRow: 'sectionColumn1GridRow',
      SectionColumn1AlignItems: 'sectionColumn1AlignItems'
    }

    let i = 0
    for (i; i < layouts.length; i++) {
      ResponsiveLayout.applyProperties(layouts[i], breakpoint, customProperties)
    }
  }

  ResponsiveLayout.layoutsRegister('one', oneColumn)

})(ResponsiveLayout)
