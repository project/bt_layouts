((ResponsiveLayout) => {
  twoColumns = function(layouts, breakpoint) {
    let customProperties = {
      SectionColumnsSize: 'sectionColumnsSize',
      SectionColumnsGap: 'sectionColumnsGap',
      SectionColumn1Order: 'sectionColumn1Order',
      SectionColumn1GridRow: 'sectionColumn1GridRow',
      SectionColumn1AlignItems: 'sectionColumn1AlignItems',
      SectionColumn2Order: 'sectionColumn2Order',
      SectionColumn2GridRow: 'sectionColumn2GridRow',
      SectionColumn2AlignItems: 'sectionColumn2AlignItems'
    }

    let i = 0
    for (i; i < layouts.length; i++) {
      ResponsiveLayout.applyProperties(layouts[i], breakpoint, customProperties)
    }
  }

  ResponsiveLayout.layoutsRegister('two', twoColumns)

})(ResponsiveLayout)
