<?php

declare(strict_types=1);

namespace Drupal\bt_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a layout for five columns.
 */
class LayoutSix extends LayoutFive {

  /**
   * The number of columns.
   *
   * @var numberColumns
   */
  protected $numberColumns = 6;

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    $build = parent::build($regions);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default = parent::defaultConfiguration();
    $screens = $this->getBreakpointsOptions();

    $default['column_6_background_color'] = 'transparent';
    $default['column_6_background_custom_color'] = '#000000';

    foreach ($screens as $prefix => $breakpoint) {
      if ($prefix == 'xs') {
        $default[$prefix . '_columns_size'] = '1fr';
      }
      elseif ($prefix == 'md') {
        $default[$prefix . '_columns_size'] = '1fr 1fr 1fr 1fr 1fr 1fr';
      }
      else {
        $default[$prefix . '_columns_size'] = 'default';
      }
      $default[$prefix . '_column_6_order'] = 'default';
      $default[$prefix . '_column_6_grid_row'] = '';
      $default[$prefix . '_column_6_align_items'] = 'center';
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $screens = $this->getBreakpointsOptions();
    $background = $this->backgroundColorBuilder();

    $form['section']['columns']['column_6'] = [
      '#type' => 'details',
      '#title' => $this->t('Column 6'),
      '#tree' => TRUE,
      '#weight' => 20,
      'background_color' => $background['color'],
      'background_custom_color' => $background['custom_color'],
    ];
    $form['section']['columns']['column_6']['background_color']['#weight'] = 0;
    $form['section']['columns']['column_6']['background_color']['#default_value'] = $this->configurations['column_1_background_color'];
    $form['section']['columns']['column_6']['background_custom_color']['#weight'] = 1;
    $form['section']['columns']['column_6']['background_custom_color']['#default_value'] = $this->configurations['column_1_background_custom_color'];
    $form['section']['columns']['column_6']['background_custom_color']['#states'] = [
      'visible' => [
        ':input[name="layout_settings[section][columns][column_6][background_color]"]' => ['value' => 'customColor'],
      ],
    ];

    foreach ($screens as $prefix => $breakpoint) {
      $form['breakpoints'][$prefix]['columns']['column_6'] = $this->columnConfigBuilder();
      $form['breakpoints'][$prefix]['columns']['column_6']['#title'] = $this->t('Column 6');
      $form['breakpoints'][$prefix]['columns']['column_6']['order']['#default_value'] = $this->configuration[$prefix . '_column_6_order'];
      $form['breakpoints'][$prefix]['columns']['column_6']['grid_row']['#default_value'] = $this->configuration[$prefix . '_column_6_grid_row'];
      $form['breakpoints'][$prefix]['columns']['column_6']['align_items']['#default_value'] = $this->configuration[$prefix . '_column_6_align_items'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['column_6_background_color'] = $values['section']['columns']['column_6']['background_color'];
    $this->configuration['column_6_background_custom_color'] = $values['section']['columns']['column_6']['background_custom_color'];

    foreach (['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as $prefix) {
      $this->configuration[$prefix . '_column_6_order'] = $values['breakpoints'][$prefix]['columns']['column_6']['order'];
      $this->configuration[$prefix . '_column_6_grid_row'] = $values['breakpoints'][$prefix]['columns']['column_6']['grid_row'];
      $this->configuration[$prefix . '_column_6_align_items'] = $values['breakpoints'][$prefix]['columns']['column_6']['align_items'];
    }
  }

  /**
   * Columns options.
   *
   * @return array
   *   The column list options.
   */
  protected function getColumnsSizeOptions(): array {
    return [
      'default' => $this->t('Default'),
      '1fr' => $this->t('Stack'),
      '1fr 1fr 1fr 1fr 1fr 1fr' => $this->t('All columns the same size.'),
    ];
  }

}
