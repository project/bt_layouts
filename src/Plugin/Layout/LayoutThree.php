<?php

declare(strict_types=1);

namespace Drupal\bt_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a layout for three columns.
 */
class LayoutThree extends LayoutTwo {

  /**
   * The number of columns.
   *
   * @var numberColumns
   */
  protected $numberColumns = 3;

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    $build = parent::build($regions);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default = parent::defaultConfiguration();
    $screens = $this->getBreakpointsOptions();

    $default['column_3_background_color'] = 'transparent';
    $default['column_3_background_custom_color'] = '#000000';

    foreach ($screens as $prefix => $breakpoint) {
      if ($prefix == 'xs') {
        $default[$prefix . '_columns_size'] = '1fr';
      }
      elseif ($prefix == 'md') {
        $default[$prefix . '_columns_size'] = '3.3fr 3.3fr 3.3fr';
      }
      else {
        $default[$prefix . '_columns_size'] = 'default';
      }
      $default[$prefix . '_column_3_order'] = 'default';
      $default[$prefix . '_column_3_grid_row'] = '';
      $default[$prefix . '_column_3_align_items'] = 'center';
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $screens = $this->getBreakpointsOptions();
    $background = $this->backgroundColorBuilder();

    $form['section']['columns']['column_3'] = [
      '#type' => 'details',
      '#title' => $this->t('Column 3'),
      '#tree' => TRUE,
      '#weight' => 20,
      'background_color' => $background['color'],
      'background_custom_color' => $background['custom_color'],
    ];
    $form['section']['columns']['column_3']['background_color']['#weight'] = 0;
    $form['section']['columns']['column_3']['background_color']['#default_value'] = $this->configuration['column_3_background_color'];
    $form['section']['columns']['column_3']['background_custom_color']['#weight'] = 1;
    $form['section']['columns']['column_3']['background_custom_color']['#default_value'] = $this->configuration['column_3_background_custom_color'];
    $form['section']['columns']['column_3']['background_custom_color']['#states'] = [
      'visible' => [
        ':input[name="layout_settings[section][columns][column_3][background_color]"]' => ['value' => 'customColor'],
      ],
    ];

    foreach ($screens as $prefix => $breakpoint) {
      $form['breakpoints'][$prefix]['columns']['column_3'] = $this->columnConfigBuilder();
      $form['breakpoints'][$prefix]['columns']['column_3']['#title'] = $this->t('Column 3');
      $form['breakpoints'][$prefix]['columns']['column_3']['order']['#default_value'] = $this->configuration[$prefix . '_column_3_order'];
      $form['breakpoints'][$prefix]['columns']['column_3']['grid_row']['#default_value'] = $this->configuration[$prefix . '_column_3_grid_row'];
      $form['breakpoints'][$prefix]['columns']['column_3']['align_items']['#default_value'] = $this->configuration[$prefix . '_column_3_align_items'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['column_3_background_color'] = $values['section']['columns']['column_3']['background_color'];
    $this->configuration['column_3_background_custom_color'] = $values['section']['columns']['column_3']['background_custom_color'];

    foreach (['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as $prefix) {
      $this->configuration[$prefix . '_column_3_order'] = $values['breakpoints'][$prefix]['columns']['column_3']['order'];
      $this->configuration[$prefix . '_column_3_grid_row'] = $values['breakpoints'][$prefix]['columns']['column_3']['grid_row'];
      $this->configuration[$prefix . '_column_3_align_items'] = $values['breakpoints'][$prefix]['columns']['column_3']['align_items'];
    }
  }

  /**
   * Columns options.
   *
   * @return array
   *   The column list options.
   */
  protected function getColumnsSizeOptions(): array {
    return [
      'default' => $this->t('Default'),
      '1fr' => $this->t('Stack'),
      '3.3fr 3.3fr 3.3fr' => $this->t('All columns the same size.'),
      '3fr 3fr 4fr' => '30% 30% 40%',
      '3fr 4fr 3fr' => '30% 40% 30%',
      '3fr 5fr 2fr' => '30% 50% 20%',
      '4fr 3fr 3fr' => '40% 30% 30%',
      '4fr 4fr 2fr' => '40% 40% 20%',
      '4fr 2fr 4fr' => '40% 20% 40%',
      '2fr 4fr 4fr' => '20% 40% 40%',
      '2fr 3fr 50fr' => '20% 30% 50%',
      '2fr 2fr 6fr' => '20% 20% 60%',
      '2fr 5fr 3fr' => '20% 50% 30%',
      '2fr 6fr 2fr' => '20% 60% 20%',
      '5fr 3fr 2fr' => '50% 30% 20%',
      '5fr 2fr 3fr' => '50% 20% 30%',
      '6fr 2fr 2fr' => '60% 20% 20%',
      '2.5fr 5fr 2.5fr' => '25% 50% 25%',
    ];
  }

}
