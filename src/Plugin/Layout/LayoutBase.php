<?php

declare(strict_types=1);

namespace Drupal\bt_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;

/**
 * Provides a layout base for custom layouts.
 */
class LayoutBase extends LayoutDefault {

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    $build = parent::build($regions);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default = [
      'label' => '',
      'title' => '',
      'title_hide' => FALSE,
      'title_font_family' => 'default',
      'title_color' => 'default',
      'title_custom_color' => '#000000',
      'title_css_classes' => '',
      'background_color' => 'transparent',
      'background_custom_color' => '#000000',
      'use_background_image' => FALSE,
      'background_image' => '',
      'css_id' => '',
      'css_classes' => '',
    ];

    foreach (['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as $prefix) {
      $default[$prefix . '_title_font_size'] = '';
      $default[$prefix . '_title_align'] = 'default';
      $default[$prefix . '_title_max_width'] = '';
      $default[$prefix . '_title_margin'] = '';
      $default[$prefix . '_container_select'] = 'full';
      $default[$prefix . '_full_select'] = 'default';
      $default[$prefix . '_box_select'] = 'default';
      $default[$prefix . '_height'] = '';
      $default[$prefix . '_padding_top'] = '';
      $default[$prefix . '_padding_bottom'] = '';
    }

    return $default;

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['section'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Section'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 1,
    ];

    $form['section']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['title'],
      '#weight' => 1,
    ];

    $form['section']['title_hide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Title'),
      '#default_value' => $this->configuration['title_hide'] ? $this->configuration['title_hide'] : FALSE,
      '#weight' => 2,
      '#states' => [
        'invisible' => [
          ':input[name="layout_settings[section][title]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['section']['title_font_family'] = [
      '#type' => 'select',
      '#options' => $this->getFontFamilyOptions(),
      '#title' => $this->t("Title Font Family"),
      '#weight' => 3,
      '#default_value' => $this->configuration['title_font_family'],
      '#states' => [
        'invisible' => [
          ':input[name="layout_settings[section][title]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['section']['title_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Title Color'),
      '#options' => $this->getThemeColorOptions(),
      '#weight' => 4,
      '#default_value' => $this->configuration['title_color'],
      '#states' => [
        'invisible' => [
          ':input[name="layout_settings[section][title]"]' => ['value' => ''],
        ],
      ],
    ];

    $form['section']['title_custom_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Custom title color'),
      '#description' => $this->t("Select a custom section title color."),
      '#default_value' => $this->configuration['title_custom_color'],
      '#weight' => 5,
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[section][title_color]"]' => ['value' => 'customColor'],
          ':input[name="layout_settings[section][title]"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['section']['title_css_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title CSS class(es)'),
      '#description' => $this->t('Customize the styling of the title by adding CSS classes. Separate multiple classes by spaces and do not include a period.'),
      '#default_value' => $this->configuration['title_css_classes'],
      '#weight' => 6,
    ];

    $background = $this->backgroundColorBuilder();
    $form['section']['background_color'] = $background['color'];
    $form['section']['background_color']['#weight'] = 6;
    $form['section']['background_color']['#default_value'] = $this->configuration['background_color'];
    $form['section']['background_custom_color'] = $background['custom_color'];
    $form['section']['background_custom_color']['#weight'] = 7;
    $form['section']['background_custom_color']['#default_value'] = $this->configuration['background_custom_color'];
    $form['section']['background_custom_color']['#states'] = [
      'visible' => [
        ':input[name="layout_settings[section][background_color]"]' => ['value' => 'customColor'],
      ],
    ];

    $form['section']['use_background_image'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use an image as background'),
      '#default_value' => $this->configuration['use_background_image'] ? $this->configuration['use_background_image'] : FALSE,
      '#weight' => 8,
    ];

    $form['section']['background_image'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['bt_image'],
      '#title' => $this->t('Upload your image'),
      '#default_value' => $this->configuration['background_image'] ?? NULL,
      '#description' => $this->t('Upload or select your profile image.'),
      '#weight' => 9,
      '#cardinality' => 1,
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[section][use_background_image]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['section']['css_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS ID'),
      '#description' => $this->t('Customize the styling of this block by adding CSS ID.'),
      '#default_value' => $this->configuration['css_id'],
      '#weight' => 10,
    ];

    $form['section']['css_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS class(es)'),
      '#description' => $this->t('Customize the styling of this section by adding CSS classes. Separate multiple classes by spaces and do not include a period.'),
      '#default_value' => $this->configuration['css_classes'],
      '#weight' => 11,
    ];

    $screens = $this->getBreakpointsOptions();

    $form['breakpoints'] = [
      '#type' => 'details',
      '#title' => $this->t('Breakpoints'),
      '#tree' => TRUE,
      '#weight' => 20,
    ];

    foreach ($screens as $prefix => $breakpoint) {
      $form['breakpoints'][$prefix] = [
        '#type' => 'details',
        '#title' => $breakpoint,
        '#tree' => TRUE,
      ];

      $form['breakpoints'][$prefix]['section'] = [
        '#type' => 'details',
        '#title' => $this->t('Title'),
        '#tree' => TRUE,
        '#states' => [
          'invisible' => [
            ':input[name="layout_settings[section][title]"]' => ['value' => ''],
          ],
        ],
      ];

      $form['breakpoints'][$prefix]['section'][$prefix . '_title_font_size'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title Font Size'),
        '#description' => $this->t('Units: px, em, rem.'),
        '#default_value' => $this->configuration[$prefix . '_title_font_size'],
        '#maxlength' => 6,
        '#weight' => 1,
      ];

      $form['breakpoints'][$prefix]['section'][$prefix . '_title_align'] = [
        '#type' => 'select',
        '#title' => $this->t('Title Align'),
        '#options' => [
          'default' => $this->t('Default'),
          'left' => $this->t('Left'),
          'center' => $this->t('Center'),
          'right' => $this->t('Right'),
        ],
        '#default_value' => $this->configuration[$prefix . '_title_align'],
        '#weight' => 2,
      ];

      $form['breakpoints'][$prefix]['section'][$prefix . '_title_max_width'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title Max Width'),
        '#description' => $this->t('Units: px, em, rem.'),
        '#default_value' => $this->configuration[$prefix . '_title_max_width'],
        '#maxlength' => 5,
        '#weight' => 1,
      ];

      $form['breakpoints'][$prefix]['section'][$prefix . '_title_margin'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title Margin'),
        '#description' => $this->t('Units: px, em, rem. Example for top 2rem, right 3rem, bottom 2rem and left 3rem: 2rem 3rem 2rem 3rem'),
        '#default_value' => $this->configuration[$prefix . '_title_margin'],
        '#maxlength' => 30,
        '#weight' => 1,
      ];

      $form['breakpoints'][$prefix]['container'] = [
        '#type' => 'details',
        '#title' => $this->t('Container'),
        '#tree' => TRUE,
      ];

      $form['breakpoints'][$prefix]['container'][$prefix . '_container_select'] = [
        '#type' => 'select',
        '#options' => [
          'full' => $this->t('Full'),
          'box' => $this->t('Box'),
        ],
        '#title' => $this->t('Container Type'),
        '#description' => $this->t("Select a container type for the width."),
        '#default_value' => $this->configuration[$prefix . '_container_select'],
        '#weight' => 1,
      ];

      $form['breakpoints'][$prefix]['container'][$prefix . '_full_select'] = [
        '#type' => 'select',
        '#options' => [
          'default' => $this->t('Default'),
          '0' => $this->t('100%'),
          '2.5%' => $this->t('95%'),
          '5%' => $this->t('90%'),
          '7.5%' => $this->t('85%'),
          '10%' => $this->t('80%'),
          '12.5%' => $this->t('75%'),
          '15%' => $this->t('70%'),
          '17.5%' => $this->t('65%'),
          '20%' => $this->t('60%'),
          '22.5%' => $this->t('55%'),
          '25%' => $this->t('50%'),
        ],
        '#title' => $this->t('Full Container Width'),
        '#description' => $this->t("Select a width."),
        '#default_value' => $this->configuration[$prefix . '_full_select'],
        '#states' => [
          'visible' => [
            ':input[name="layout_settings[breakpoints][' . $prefix . '][container][' . $prefix . '_container_select]"]' => ['value' => 'full'],
          ],
        ],
        '#weight' => 2,
      ];

      $form['breakpoints'][$prefix]['container'][$prefix . '_box_select'] = [
        '#type' => 'select',
        '#options' => [
          'default' => $this->t('Default'),
          '100%' => $this->t('100%'),
          '95%' => $this->t('95%'),
          '90%' => $this->t('90%'),
          '85%' => $this->t('85%'),
          '80%' => $this->t('80%'),
          '75%' => $this->t('75%'),
          '70%' => $this->t('70%'),
          '65%' => $this->t('65%'),
          '60%' => $this->t('60%'),
          '55%' => $this->t('55%'),
          '50%' => $this->t('50%'),
        ],
        '#title' => $this->t('Box Container Width'),
        '#description' => $this->t("Select a width."),
        '#default_value' => $this->configuration[$prefix . '_box_select'],
        '#states' => [
          'visible' => [
            ':input[name="layout_settings[breakpoints][' . $prefix . '][container][' . $prefix . '_container_select]"]' => ['value' => 'box'],
          ],
        ],
        '#weight' => 3,
      ];

      $form['breakpoints'][$prefix]['container'][$prefix . '_height'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Height'),
        '#description' => $this->t('Enter a height. Examples: 100vh, 400px.'),
        '#default_value' => $this->configuration[$prefix . '_height'] ? $this->configuration[$prefix . '_height'] : '',
        '#maxlength' => 11,
        '#weight' => 4,
      ];

      $form['breakpoints'][$prefix]['container']['padding'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Padding'),
        '#description' => $this->t('Add padding to section. Units: px, rem, vh. Example: 15px'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#weight' => 5,
      ];

      $form['breakpoints'][$prefix]['container']['padding'][$prefix . '_padding_top'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Padding Top'),
        '#default_value' => $this->configuration[$prefix . '_padding_top'],
        '#maxlength' => 5,
        '#weight' => 1,
      ];

      $form['breakpoints'][$prefix]['container']['padding'][$prefix . '_padding_bottom'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Padding Bottom'),
        '#default_value' => $this->configuration[$prefix . '_padding_bottom'],
        '#maxlength' => 5,
        '#weight' => 2,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['label'] = $values['label'];
    $this->configuration['title'] = $values['section']['title'];
    $this->configuration['title_font_family'] = $values['section']['title_font_family'];
    $this->configuration['title_color'] = $values['section']['title_color'];
    $this->configuration['title_custom_color'] = $values['section']['title_custom_color'];
    $this->configuration['title_hide'] = $values['section']['title_hide'];
    $this->configuration['title_css_classes'] = $values['section']['title_css_classes'];
    $this->configuration['background_color'] = $values['section']['background_color'];
    $this->configuration['background_custom_color'] = $values['section']['background_custom_color'];
    $this->configuration['use_background_image'] = $values['section']['use_background_image'];
    $this->configuration['background_image'] = $values['section']['background_image'];
    $this->configuration['css_id'] = $values['section']['css_id'];
    $this->configuration['css_classes'] = $values['section']['css_classes'];

    foreach (['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as $prefix) {
      $this->configuration[$prefix . '_title_font_size'] = $values['breakpoints'][$prefix]['section'][$prefix . '_title_font_size'];
      $this->configuration[$prefix . '_title_align'] = $values['breakpoints'][$prefix]['section'][$prefix . '_title_align'];
      $this->configuration[$prefix . '_title_max_width'] = $values['breakpoints'][$prefix]['section'][$prefix . '_title_max_width'];
      $this->configuration[$prefix . '_title_margin'] = $values['breakpoints'][$prefix]['section'][$prefix . '_title_margin'];
      $this->configuration[$prefix . '_container_select'] = $values['breakpoints'][$prefix]['container'][$prefix . '_container_select'];
      $this->configuration[$prefix . '_full_select'] = $values['breakpoints'][$prefix]['container'][$prefix . '_full_select'];
      $this->configuration[$prefix . '_box_select'] = $values['breakpoints'][$prefix]['container'][$prefix . '_box_select'];
      $this->configuration[$prefix . '_height'] = $values['breakpoints'][$prefix]['container'][$prefix . '_height'];
      $this->configuration[$prefix . '_padding_top'] = $values['breakpoints'][$prefix]['container']['padding'][$prefix . '_padding_top'];
      $this->configuration[$prefix . '_padding_bottom'] = $values['breakpoints'][$prefix]['container']['padding'][$prefix . '_padding_bottom'];
    }
  }

  /**
   * Return an form section for column region configurations.
   *
   * @return array
   *   The form column config.
   */
  protected function columnConfigBuilder() :array {
    $column = [
      '#type' => 'details',
      '#tree' => TRUE,
      'grid_row' => [
        '#type' => 'textfield',
        '#title' => $this->t('CSS Grid Template Rows'),
        '#description' => $this->t('Set the grid-template-rows CSS property for this column.'),
        '#weight' => 5,
      ],
      'align_items' => [
        '#type' => 'select',
        '#options' => $this->getAlignItemsOptions(),
        '#title' => $this->t("Align items"),
        '#weight' => 6,
      ],
    ];

    return $column;
  }

  /**
   * Return an form section for column order configurations.
   *
   * @return array
   *   The form column order config.
   */
  protected function columnOrderBuilder($columns) : array {
    $options = [
      'default' => 'Default',
    ];

    for ($i = 1; $i <= $columns; $i++) {
      $options[$i] = $i;
    }

    $order = [
      '#type' => 'select',
      '#title' => $this->t('Column Order'),
      '#options' => $options,
      '#description' => $this->t("Column Order"),
      '#weight' => 2,
    ];

    return $order;
  }

  /**
   * Return an form section for column gap configurations.
   *
   * @return array
   *   The form column gap config.
   */
  protected function columnsGapBuilder() : array {
    $gap = [
      '#type' => 'textfield',
      '#title' => $this->t('Columns Gap'),
      '#weight' => 0,
      '#description' => $this->t("Units: px, vw, rem. Example: 1rem"),
    ];

    return $gap;
  }

  /**
   * Return an form section for column background color configurations.
   *
   * @return array
   *   The form column background color config.
   */
  protected function backgroundColorBuilder() : array {
    $background = [];
    $background['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Background Color'),
      '#options' => $this->getThemeColorOptions(),
    ];

    $background['custom_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Custom background color'),
      '#description' => $this->t("Select a custom color as background."),
      '#default_value' => $this->configuration['background_custom_color'],
    ];

    return $background;
  }

  /**
   * Return an form section for color element configurations.
   *
   * @return array
   *   The form color select.
   */
  protected function colorSelectBuilder() {
    return [
      'color_select' => [
        '#type' => 'select',
        '#options' => $this->getThemeColorOptions(),
        '#title' => $this->t('Color'),
        '#description' => $this->t("Select a theme color or use a custom color."),
      ],
      'color_custom' => [
        '#type' => 'color',
        '#title' => $this->t("Custom color"),
        '#description' => $this->t("Click in the color to select a custom one."),
      ],
    ];
  }

  /**
   * Return an form section for color and opacity elements configurations.
   *
   * @return array
   *   The form color opacity select.
   */
  protected function colorOpacitySelectBuilder() {
    return [
      'color_select' => [
        '#type' => 'select',
        '#options' => $this->geThemeColorOptions(),
        '#title' => $this->t('Color'),
        '#description' => $this->t("Select a theme color or use a custom color."),
      ],

      'color_custom' => [
        '#type' => 'color',
        '#title' => $this->t('Custom Color'),
        '#description' => $this->t("Click in the color to select a custom one."),
      ],

      'color_opacity' => [
        '#type' => 'select',
        '#options' => $this->getOpacityOptions('decimal'),
        '#title' => $this->t('Color Opacity'),
        '#description' => $this->t("Select an opacity for color."),
      ],

      'color_custom_opacity' => [
        '#type' => 'select',
        '#options' => $this->getOpacityOptions('hexadecimal'),
        '#title' => $this->t('Color Opacity'),
        '#description' => $this->t("Select an opacity for color."),
      ],
    ];
  }

  /**
   * Returns a list of breakpoint screen sizes.
   *
   * @return array
   *   The form breakpoints options.
   */
  protected function getBreakpointsOptions() :array {
    return [
      'xs' => $this->t('max-width: 575px'),
      'sm' => $this->t('min-width: 576px'),
      'md' => $this->t('min-width: 768px'),
      'lg' => $this->t('min-width: 992px'),
      'xl' => $this->t('min-width: 1200px'),
      'xxl' => $this->t('min-width: 1400px'),
    ];
  }

  /**
   * Return a font select options.
   *
   * @return array
   *   The form family select options.
   */
  protected function getFontFamilyOptions() :array {
    return [
      'default' => 'Default',
      'roboto' => 'Roboto',
      'jolly-lodger' => 'Jolly Lodger',
      'oswald' => 'Oswald',
      'zen-antique' => 'Zen Antique',
      'amatic-sc' => 'Amatic SC',
      'kalam' => 'Kalam',
      'russo-one' => 'Russo One',
      'archivo-black' => 'Archivo Black',
      'el-messiri' => 'El Messiri',
      'akaya-telivigala' => 'Akaya Telivigala',
      'baloo-2' => 'Baloo 2',
      'noticia-text' => 'Noticia Text',
      'kaushan-script' => 'Kaushan Script',
      'titan-one' => 'Titan One',
      'montserrat' => 'Montserrat',
      'ubuntu' => 'Ubuntu',
      'merriweather' => 'Merriweather',
      'playfair-display' => 'Playfair Display',
      'kanit' => 'Kanit',
      'lora' => 'Lora',
      'fira-sans' => 'Fira Sans',
      'quicksand' => 'Quicksand',
      'barlow' => 'Barlow',
      'titillium-web' => 'Titillium Web',
      'trispace' => 'Trispace',
      'josefin-sans' => 'Josefin Sans',
      'shalimar' => 'Shalimar',
      'anton' => 'Anton',
      'dancing-script' => 'Dancing Script',
      'secular-one' => 'Secular One',
      'lobster' => 'Lobster',
      'fjalla-one' => 'Fjalla One',
      'exo-2' => 'Exo 2',
      'caveat' => 'Caveat',
    ];
  }

  /**
   * Return a color opacity select options.
   *
   * @return array
   *   The form color opacity select options.
   */
  protected function getOpacityOptions($type) :array {
    if ($type == 'hexadecimal') {
      return [
        'FF' => "100%",
        'F2' => "95%",
        'E6' => "90%",
        'D9' => "85%",
        'CC' => "80%",
        'BF' => "75%",
        'B3' => "70%",
        'A6' => "65%",
        '99' => "60%",
        '8C' => "55%",
        '80' => "50%",
        '73' => "45%",
        '66' => "40%",
        '59' => "35%",
        '4D' => "30%",
        '40' => "25%",
        '33' => "20%",
        '26' => "15%",
        '1A' => "10%",
        '0D' => "5%",
        '00' => "0%",
      ];
    }
    elseif ($type == 'decimal') {
      return [
        '1.00' => "100%",
        '0.95' => "95%",
        '0.90' => "90%",
        '0.85' => "85%",
        '0.80' => "80%",
        '0.75' => "75%",
        '0.70' => "70%",
        '0.65' => "65%",
        '0.60' => "60%",
        '0.55' => "55%",
        '0.50' => "50%",
        '0.45' => "45%",
        '0.40' => "40%",
        '0.35' => "35%",
        '0.30' => "30%",
        '0.25' => "25%",
        '0.20' => "20%",
        '0.15' => "15%",
        '0.10' => "10%",
        '0.05' => "5%",
        '0.00' => "0%",
      ];
    }
  }

  /**
   * Return an array of theme's color name.
   *
   * @return array
   *   The form theme color select options.
   */
  protected function getThemeColorOptions() :array {
    return [
      'default' => $this->t('Default'),
      'primary' => $this->t('Primary'),
      'primary-050' => $this->t('Primary 50'),
      'primary-100' => $this->t('Primary 100'),
      'primary-200' => $this->t('Primary 200'),
      'primary-300' => $this->t('Primary 300'),
      'primary-400' => $this->t('Primary 400'),
      'primary-500' => $this->t('Primary 500'),
      'primary-600' => $this->t('Primary 600'),
      'primary-700' => $this->t('Primary 700'),
      'primary-800' => $this->t('Primary 800'),
      'primary-900' => $this->t('Primary 900'),
      'secondary' => $this->t('Secondary'),
      'secondary-050' => $this->t('Secondary 50'),
      'secondary-100' => $this->t('Secondary 100'),
      'secondary-200' => $this->t('Secondary 200'),
      'secondary-300' => $this->t('Secondary 300'),
      'secondary-400' => $this->t('Secondary 400'),
      'secondary-500' => $this->t('Secondary 500'),
      'secondary-600' => $this->t('Secondary 600'),
      'secondary-700' => $this->t('Secondary 700'),
      'secondary-800' => $this->t('Secondary 800'),
      'secondary-900' => $this->t('Secondary 900'),
      'accent' => $this->t('Accent'),
      'accent-050' => $this->t('Accent 50'),
      'accent-100' => $this->t('Accent 100'),
      'accent-200' => $this->t('Accent 200'),
      'accent-300' => $this->t('Accent 300'),
      'accent-400' => $this->t('Accent 400'),
      'accent-500' => $this->t('Accent 500'),
      'accent-600' => $this->t('Accent 600'),
      'accent-700' => $this->t('Accent 700'),
      'accent-800' => $this->t('Accent 800'),
      'accent-900' => $this->t('Accent 900'),
      'success' => $this->t('Success'),
      'info' => $this->t('Info'),
      'warning' => $this->t('Warning'),
      'danger' => $this->t('Danger'),
      'light' => $this->t('Light'),
      'dark' => $this->t('Dark'),
      'blue' => $this->t('Blue'),
      'indigo' => $this->t('Indigo'),
      'purple' => $this->t('Purple'),
      'pink' => $this->t('Pink'),
      'red' => $this->t('Red'),
      'orange' => $this->t('Orange'),
      'yellow' => $this->t('Yellow'),
      'green' => $this->t('Green'),
      'teal' => $this->t('Teal'),
      'cyan' => $this->t('Cyan'),
      'white' => $this->t('White'),
      'gray-050' => $this->t('Gray 50'),
      'gray-100' => $this->t('Gray 100'),
      'gray-200' => $this->t('Gray 200'),
      'gray-300' => $this->t('Gray 300'),
      'gray-400' => $this->t('Gray 400'),
      'gray-500' => $this->t('Gray 500'),
      'gray-600' => $this->t('Gray 600'),
      'gray-700' => $this->t('Gray 700'),
      'gray-800' => $this->t('Gray 800'),
      'gray-900' => $this->t('Gray 900'),
      'customColor' => $this->t('Custom Color'),
    ];
  }

  /**
   * Return an array of sizes to apply in block styles form.
   *
   * @return array
   *   The form size options.
   */
  protected function getSizeOptions() :array {
    return [
      'default' => $this->t('Default'),
      'none' => $this->t('None'),
      '0.5rem' => $this->t('Half'),
      '' => $this->t('Normal'),
      '2rem' => $this->t('Double'),
      '3rem' => $this->t('Triple'),
    ];
  }

  /**
   * Return an animations select options.
   *
   * @return array
   *   The form animation options.
   */
  protected function getAnimationOptions() :array {
    return [
      'bounce' => 'Bounce',
      'flash' => 'Flash',
      'jello' => 'Jello',
      'pulse' => 'Pulse',
      'rubberBand' => 'Rubber Band',
      'shake' => 'Shake',
      'swing' => 'Swing',
      'tada' => 'Tada',
      'wooble' => 'Wooble',
      'bounceIn' => 'Bounce In',
      'bounceInDown' => 'Bounce In Down',
      'bounceInLeft' => 'Bounce In Left',
      'bounceInRight' => 'Bounce In Right',
      'bounceInUp' => 'Bounce In UP',
      'bounceOut' => 'Bounce Out',
      'bounceOutDown' => 'Bounce Out Down',
      'bounceOutLeft' => 'Bounce Out Left',
      'bounceOutRight' => 'Bounce Out Right',
      'bounceOutUp' => 'Bounce Out UP',
      'fadeIn' => 'Fade In',
      'fadeInDown' => 'Fade In Down',
      'fadeInDownBig' => 'Fade In Down Big',
      'fadeInLeft' => 'Fade In Left',
      'fadeInLeftBig' => 'Fade In Left Big',
      'fadeInRight' => 'Fade In Right',
      'fadeInRightBig' => 'Fade In Right Big',
      'fadeInUp' => 'Fade In Up',
      'fadeInUpBig' => 'Fade In Up Big',
      'fadeOut' => 'Fade Out',
      'fadeOutDown' => 'Fade Out Down',
      'fadeOutDownBig' => 'Fade Out Down Big',
      'fadeOutLeft' => 'Fade Out Left',
      'fadeOutLeftBig' => 'Fade Out Left Big',
      'fadeOutRight' => 'Fade Out Right',
      'fadeOutRightBig' => 'Fade Out Right Big',
      'fadeOutUp' => 'Fade Out Up',
      'fadeOutUpBig' => 'Fade Out Up Big',
      'flip' => 'Flip',
      'flipInX' => 'Flip In X',
      'flipInY' => 'Flip In Y',
      'flipOutX' => 'Flip Out X',
      'flipOutY' => 'Flip Out Y',
      'lightSpeedIn' => 'Light Speed In',
      'lightSpeedOut' => 'Light Speed OUT',
      'rotateIn' => 'Rotate In',
      'rotateInDownLeft' => 'Rotate In Down Left',
      'rotateInDownRight' => 'Rotate In Down Right',
      'rotateInUpLeft' => 'Rotate In Up Left',
      'rotateInUpRight' => 'Rotate In Up Right',
      'rotateOut' => 'Rotate Out',
      'rotateOutDownLeft' => 'Rotate Out Down Left',
      'rotateOutDownRight' => 'Rotate Out Down Right',
      'rotateOutUpLeft' => 'Rotate Out Up Left',
      'rotateOutUpRight' => 'Rotate Out Up Right',
      'slideInDown' => 'Slide In Down',
      'slideInLeft' => 'Slide In Left',
      'slideInRight' => 'Slide In Right',
      'slideOutUp' => 'Slide Out Up',
      'slideOutDown' => 'Slide Out Down',
      'slideOutLeft' => 'Slide Out Left',
      'slideOutRight' => 'Slide Out Right',
      'slideOutUp' => 'Slide Out Up',
      'hinge' => 'Hinge',
      'rollIn' => 'Roll In',
      'rollOut' => 'Roll Out',
      'zoomIn' => 'Zoom In',
      'zoomInDown' => 'Zoom In Down',
      'zoomInLeft' => 'Zoom In Left',
      'zoomInRight' => 'Zoom In Right',
      'zoomInUp' => 'Zoom In Up',
      'zoomOut' => 'Zoom Out',
      'zoomOutDown' => 'Zoom Out Down',
      'zoomOutLeft' => 'Zoom Out Left',
      'zoomOutRight' => 'Zoom Out Right',
      'zoomOutUp' => 'Zoom Out Up',
    ];
  }

  /**
   * Returns a list of CSS grid align items options.
   *
   * @return array
   *   The form forn align items options.
   */
  protected function getAlignItemsOptions() :array {
    return [
      'default' => $this->t('Default'),
      'auto' => $this->t('Auto'),
      'baseline' => $this->t('Baseline'),
      'center' => $this->t('Center'),
      'end' => $this->t('End'),
      'first-baseline' => $this->t('First Baseline'),
      'flex-end' => $this->t('Flex End'),
      'flex-start' => $this->t('Flex Start'),
      'inherit' => $this->t('Inherit'),
      'initial' => $this->t('Initial'),
      'last-baseline' => $this->t('Last Baseline'),
      'left' => $this->t('Left'),
      'normal' => $this->t('Normal'),
      'revert' => $this->t('Revert'),
      'revert-layer' => $this->t('Revert Layer'),
      'right' => $this->t('Right'),
      'safe' => $this->t('Safe'),
      'self-end' => $this->t('Self End'),
      'self-start' => $this->t('Self Start'),
      'start' => $this->t('Start'),
      'stretch' => $this->t('Stretch'),
      'unsafe' => $this->t('Unsafe'),
      'unset' => $this->t('Unset'),
    ];
  }

  /**
   * Return a block style form element configurations for margin and padding.
   */
  protected function breakpointsMarginPaddinBuilder($breakpoint, $type) {
    $return = [
      '#type' => 'details',
      '#title' => $breakpoint,
      '#tree' => TRUE,
    ];

    $sides = [
      'left' => $this->t('Left'),
      'top' => $this->t('Top'),
      'right' => $this->t('Right'),
      'bottom' => $this->t('Bottom'),
    ];

    foreach ($sides as $side => $title) {
      $return[$type . '_' . $side] = [
        '#type' => 'textfield',
        '#title' => $title,
        '#maxlength' => '6',
        '#description' => $this->t("Units: px, rem, vw. Examples: 2rem, 0.5vw"),
      ];
    }

    return $return;

  }

  /**
   * Return an block style form element configurations for align element.
   */
  protected function breakpointsAlignBuilder($breakpoint) {
    $block_align = [
      '#type' => 'details',
      '#title' => $breakpoint,
      '#tree' => TRUE,
    ];

    $block_align['align'] = [
      '#type' => 'select',
      '#title' => $this->t('Align'),
      '#options' => [
        'default' => $this->t('Default'),
        'auto' => $this->t('Auto'),
        'baseline' => $this->t('Baseline'),
        'center' => $this->t('Center'),
        'end' => $this->t('End'),
        'first-baseline' => $this->t('First Baseline'),
        'flex-end' => $this->t('Flex End'),
        'flex-start' => $this->t('Flex Start'),
        'inherit' => $this->t('Inherit'),
        'initial' => $this->t('Initial'),
        'last-baseline' => $this->t('Last Baseline'),
        'left' => $this->t('Left'),
        'normal' => $this->t('Normal'),
        'revert' => $this->t('Revert'),
        'revert-layer' => $this->t('Revert Layer'),
        'right' => $this->t('Right'),
        'safe' => $this->t('Safe'),
        'self-end' => $this->t('Self End'),
        'self-start' => $this->t('Self Start'),
        'start' => $this->t('Start'),
        'stretch' => $this->t('Stretch'),
        'unsafe' => $this->t('Unsafe'),
        'unset' => $this->t('Unset'),
      ],
      '#description' => $this->t("Align"),
    ];

    return $block_align;
  }

  /**
   * Return an block style form element configurations for justify element.
   */
  protected function breakpointsGridJustifyBuilder($breakpoint) {
    $block_align = [
      '#type' => 'details',
      '#title' => $breakpoint,
      '#tree' => TRUE,
    ];

    $block_align['justify'] = [
      '#type' => 'select',
      '#title' => $this->t('Justify'),
      '#options' => [
        'default' => $this->t('Default'),
        'center' => $this->t('Center'),
        'end' => $this->t('End'),
        'flex-end' => $this->t('Flex End'),
        'flex-start' => $this->t('Flex Start'),
        'inherit' => $this->t('Inherit'),
        'initial' => $this->t('Initial'),
        'left' => $this->t('Left'),
        'normal' => $this->t('Normal'),
        'revert' => $this->t('Revert'),
        'revert-layer' => $this->t('Revert Layer'),
        'right' => $this->t('Right'),
        'safe' => $this->t('Safe'),
        'start' => $this->t('Start'),
        'stretch' => $this->t('Stretch'),
        'unsafe' => $this->t('Unsafe'),
        'unset' => $this->t('Unset'),
      ],
      '#description' => $this->t("Justify"),
    ];

    return $block_align;
  }

  /**
   * Return an block style form element configurations for text align element.
   */
  protected function breakpointsTextAlignBuilder($breakpoint) {
    $text_align = [
      '#type' => 'details',
      '#title' => $breakpoint,
      '#tree' => TRUE,
    ];
    $text_align['text_align'] = [
      '#type' => 'select',
      '#title' => $this->t('Text align'),
      '#options' => [
        'default' => $this->t('Default'),
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
      '#description' => $this->t("Block's text align"),
    ];

    return $text_align;
  }

  /**
   * Return an block style form element configurations for font size element.
   */
  protected function breakpointsFontSizeBuilder($breakpoint) {
    $font_size = [
      '#type' => 'details',
      '#title' => $breakpoint,
      '#tree' => TRUE,
    ];
    $font_size['font_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Font size.'),
      '#maxlength' => '7',
    ];

    return $font_size;
  }

  /**
   * Return an block style form element configurations for max_width element.
   */
  protected function breakpointsMaxWidthBuilder($breakpoint) {
    $max_width = [
      '#type' => 'details',
      '#title' => $breakpoint,
      '#tree' => TRUE,
    ];
    $max_width['max_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max width.'),
      '#maxlength' => '15',
    ];

    return $max_width;
  }

}
