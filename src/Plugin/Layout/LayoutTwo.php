<?php

declare(strict_types=1);

namespace Drupal\bt_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a layout base for custom layouts.
 */
class LayoutTwo extends LayoutBase {

  /**
   * The number of columns.
   *
   * @var numberColumns
   */
  protected $numberColumns = 2;

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    $build = parent::build($regions);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default = parent::defaultConfiguration();
    $screens = $this->getBreakpointsOptions();

    $default['column_1_background_color'] = 'transparent';
    $default['column_1_background_custom_color'] = '#000000';
    $default['column_2_background_color'] = 'transparent';
    $default['column_2_background_custom_color'] = '#000000';

    foreach ($screens as $prefix => $breakpoint) {
      if ($prefix == 'xs') {
        $default[$prefix . '_columns_size'] = '1fr';
      }
      elseif ($prefix == 'sm') {
        $default[$prefix . '_columns_size'] = '1fr 1fr';
      }
      else {
        $default[$prefix . '_columns_size'] = 'default';
      }
      $default[$prefix . '_columns_gap'] = '';
      $default[$prefix . '_column_1_order'] = 'default';
      $default[$prefix . '_column_1_grid_row'] = '';
      $default[$prefix . '_column_1_align_items'] = 'default';
      $default[$prefix . '_column_2_order'] = 'default';
      $default[$prefix . '_column_2_grid_row'] = '';
      $default[$prefix . '_column_2_align_items'] = 'default';
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $screens = $this->getBreakpointsOptions();
    $background = $this->backgroundColorBuilder();

    $form['section']['columns'] = [
      '#type' => 'details',
      '#title' => $this->t('Columns'),
      '#tree' => TRUE,
      '#weight' => 20,
    ];

    $form['section']['columns']['column_1'] = [
      '#type' => 'details',
      '#title' => $this->t('Column 1'),
      '#tree' => TRUE,
      '#weight' => 20,
      'background_color' => $background['color'],
      'background_custom_color' => $background['custom_color'],
    ];
    $form['section']['columns']['column_1']['background_color']['#weight'] = 0;
    $form['section']['columns']['column_1']['background_color']['#default_value'] = $this->configuration['column_1_background_color'];
    $form['section']['columns']['column_1']['background_custom_color']['#weight'] = 1;
    $form['section']['columns']['column_1']['background_custom_color']['#default_value'] = $this->configuration['column_1_background_custom_color'];
    $form['section']['columns']['column_1']['background_custom_color']['#states'] = [
      'visible' => [
        ':input[name="layout_settings[section][columns][column_1][background_color]"]' => ['value' => 'customColor'],
      ],
    ];

    $form['section']['columns']['column_2'] = [
      '#type' => 'details',
      '#title' => $this->t('Column 2'),
      '#tree' => TRUE,
      '#weight' => 20,
      'background_color' => $background['color'],
      'background_custom_color' => $background['custom_color'],
    ];
    $form['section']['columns']['column_2']['background_color']['#weight'] = 0;
    $form['section']['columns']['column_2']['background_color']['#default_value'] = $this->configuration['column_2_background_color'];
    $form['section']['columns']['column_2']['background_custom_color']['#weight'] = 1;
    $form['section']['columns']['column_2']['background_custom_color']['#default_value'] = $this->configuration['column_2_background_custom_color'];
    $form['section']['columns']['column_2']['background_custom_color']['#states'] = [
      'visible' => [
        ':input[name="layout_settings[section][columns][column_2][background_color]"]' => ['value' => 'customColor'],
      ],
    ];

    foreach ($screens as $prefix => $breakpoint) {
      $form['breakpoints'][$prefix]['columns'] = [
        '#title' => $this->t('Columns'),
        '#type' => 'details',
        '#tree' => TRUE,
      ];

      $form['breakpoints'][$prefix]['columns']['columns_size'] = [
        '#type' => 'select',
        '#title' => $this->t('Columns Size'),
        '#options' => $this->getColumnsSizeOptions(),
        '#weight' => -1,
        '#default_value' => $this->configuration[$prefix . '_columns_size'],
        '#required' => TRUE,
      ];

      $form['breakpoints'][$prefix]['columns']['gap'] = $this->columnsGapBuilder();
      $form['breakpoints'][$prefix]['columns']['gap']['#default_value'] = $this->configuration[$prefix . '_columns_gap'];

      $form['breakpoints'][$prefix]['columns']['column_1'] = $this->columnConfigBuilder();
      $form['breakpoints'][$prefix]['columns']['column_1']['#title'] = $this->t('Column 1');
      $form['breakpoints'][$prefix]['columns']['column_1']['order']['#default_value'] = $this->configuration[$prefix . '_column_1_order'];
      $form['breakpoints'][$prefix]['columns']['column_1']['grid_row']['#default_value'] = $this->configuration[$prefix . '_column_1_grid_row'];
      $form['breakpoints'][$prefix]['columns']['column_1']['align_items']['#default_value'] = $this->configuration[$prefix . '_column_1_align_items'];

      $form['breakpoints'][$prefix]['columns']['column_2'] = $this->columnConfigBuilder();
      $form['breakpoints'][$prefix]['columns']['column_2']['#title'] = $this->t('Column 2');
      $form['breakpoints'][$prefix]['columns']['column_2']['order']['#default_value'] = $this->configuration[$prefix . '_column_2_order'];
      $form['breakpoints'][$prefix]['columns']['column_2']['grid_row']['#default_value'] = $this->configuration[$prefix . '_column_2_grid_row'];
      $form['breakpoints'][$prefix]['columns']['column_2']['align_items']['#default_value'] = $this->configuration[$prefix . '_column_2_align_items'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['column_1_background_color'] = $values['section']['columns']['column_1']['background_color'];
    $this->configuration['column_1_background_custom_color'] = $values['section']['columns']['column_1']['background_custom_color'];
    $this->configuration['column_2_background_color'] = $values['section']['columns']['column_2']['background_color'];
    $this->configuration['column_2_background_custom_color'] = $values['section']['columns']['column_2']['background_custom_color'];

    foreach (['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as $prefix) {
      $this->configuration[$prefix . '_columns_size'] = $values['breakpoints'][$prefix]['columns']['columns_size'];
      $this->configuration[$prefix . '_columns_gap'] = $values['breakpoints'][$prefix]['columns']['gap'];
      $this->configuration[$prefix . '_column_1_order'] = $values['breakpoints'][$prefix]['columns']['column_1']['order'];
      $this->configuration[$prefix . '_column_1_grid_row'] = $values['breakpoints'][$prefix]['columns']['column_1']['grid_row'];
      $this->configuration[$prefix . '_column_1_align_items'] = $values['breakpoints'][$prefix]['columns']['column_1']['align_items'];
      $this->configuration[$prefix . '_column_2_order'] = $values['breakpoints'][$prefix]['columns']['column_2']['order'];
      $this->configuration[$prefix . '_column_2_grid_row'] = $values['breakpoints'][$prefix]['columns']['column_2']['grid_row'];
      $this->configuration[$prefix . '_column_2_align_items'] = $values['breakpoints'][$prefix]['columns']['column_2']['align_items'];
    }
  }

  /**
   * Return an form section for column region configurations.
   *
   * @return array
   *   The form column config.
   */
  protected function columnConfigBuilder() :array {
    $column = [
      '#type' => 'details',
      '#tree' => TRUE,
      'order' => $this->columnOrderBuilder($this->numberColumns),
      'grid_row' => [
        '#type' => 'textfield',
        '#title' => $this->t('CSS Grid Template Rows'),
        '#description' => $this->t('Set the grid-template-rows CSS property for this column.'),
        '#weight' => 5,
      ],
      'align_items' => [
        '#type' => 'select',
        '#options' => $this->getAlignItemsOptions(),
        '#title' => $this->t("Align items"),
        '#weight' => 6,
      ],
    ];

    return $column;
  }

  /**
   * Columns options.
   *
   * @return array
   *   The column list options.
   */
  protected function getColumnsSizeOptions(): array {
    return [
      'default' => $this->t('Default'),
      '1fr' => $this->t('Stack'),
      '1fr 1fr' => $this->t('All columns the same size.'),
      '8fr 2fr' => '80% 20%',
      '7.5fr 2.5fr' => '75% 25%',
      '7fr 3fr' => '70% 30%',
      '6fr 4fr' => '60% 40%',
      '4fr 6fr' => '40% 60%',
      '3fr 7fr' => '30% 70%',
      '2.5fr 7.5fr' => '25% 75%',
      '2fr 8fr' => '20% 80%',
    ];
  }

}
