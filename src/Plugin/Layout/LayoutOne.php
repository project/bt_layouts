<?php

declare(strict_types=1);

namespace Drupal\bt_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a layout base for custom layouts.
 */
class LayoutOne extends LayoutBase {

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    $build = parent::build($regions);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default = parent::defaultConfiguration();
    $default['column_1_grid_row'] = '';
    $default['column_1_align_items'] = 'center';
    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $screens = $this->getBreakpointsOptions();

    foreach ($screens as $prefix => $breakpoint) {
      $form['breakpoints'][$prefix]['column_1'] = $this->columnConfigBuilder();
      $form['breakpoints'][$prefix]['column_1']['#title'] = $this->t('Column 1');
      $form['breakpoints'][$prefix]['column_1']['grid_row']['#default_value'] = $this->configuration[$prefix . '_column_1_grid_row'];
      $form['breakpoints'][$prefix]['column_1']['align_items']['#default_value'] = $this->configuration[$prefix . '_column_1_align_items'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();

    foreach (['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as $prefix) {
      $this->configuration[$prefix . '_column_1_grid_row'] = $values['breakpoints'][$prefix]['column_1']['grid_row'];
      $this->configuration[$prefix . '_column_1_align_items'] = $values['breakpoints'][$prefix]['column_1']['align_items'];
    }
  }

}
